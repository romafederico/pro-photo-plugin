<?php

class Pro_Photo_Plugin_Admin {

	private $plugin_name;
	private $version;

	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	public function plugin_add_admin_menu(  ) {

		add_menu_page( 'Pro Photo Plugin', 'Pro Photo Plugin', 'manage_options', 'pro_photo_plugin', array($this, 'pro_photo_plugin_options_page'));

	}

	public function pro_photo_plugin_options_page() {
        echo '<h1>Pro Photo Plugin</h1>';
		echo '<div id="root"></div>';

	}

	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . '../static/css/main.22b89223.css', array(), $this->version, 'all' );

	}

	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . '../static/js/main.d95455fe.js', array(), $this->version, true );

	}

}
