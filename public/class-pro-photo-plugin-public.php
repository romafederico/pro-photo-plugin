<?php

class Pro_Photo_Plugin_Public {

	private $plugin_name;
	private $version;

	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	public function register_shortcodes() {
		add_shortcode( 'pro_photo_plugin', array( $this, 'pro_photo_add_shortcode') );
	}


	public function pro_photo_add_shortcode() {
		return '<div id="root"></div>';
	}

	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . '../static/css/main.4c2182f1.css', array(), $this->version, 'all' );

	}

	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . '../static/js/main.32baa49e.js', array(), $this->version, true );
		wp_localize_script( 'wp-api', 'wpApiSettings', array( 'root' => esc_url_raw( rest_url() ), 'nonce' => wp_create_nonce( 'wp_rest' ) ) );
	}

}
