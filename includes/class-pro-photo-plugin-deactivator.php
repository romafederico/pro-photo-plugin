<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://gitlab.com/romafederico
 * @since      1.0.0
 *
 * @package    Pro_Photo_Plugin
 * @subpackage Pro_Photo_Plugin/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Pro_Photo_Plugin
 * @subpackage Pro_Photo_Plugin/includes
 * @author     Federico Roma <romafederico@gmail.com>
 */
class Pro_Photo_Plugin_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
